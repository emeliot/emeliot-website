---
title: Meetings
links:
  - title: Kickoff Meeting
    description: Project EMELIOT kickoff meeting.
    website: /p/kickoff-meeting
    image:
menu:
    main: 
        weight: -50
        params:
            icon: messages

comments: false
---

Links to the project meetings dedicated pages.