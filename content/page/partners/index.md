---
title: Partners
description: Project EMELIOT partners
date: '2022-07-22'
license: CC BY-NC-ND
lastmod: '2022-07-22'
menu:
    main: 
        weight: -80
        params:
            icon: user
---

- Università degli Studi di Milano-Bicocca (Coordinator) _(PI: Prof. Leonardo Mariani)_
- Politecnico di Milano (Partner) _(PI: Prof. Luciano Baresi)_
- Unisersità degli Studi dell'Aquila (Partner) _(PI: Prof. Davide Di Ruscio)_
- Università degli Studi del Sannio (Partner) _(PI: Prof. Massimiliano di Penta)_
- Università degli Studi di Salerno (Partner) _(PI: Prof. Andrea De Lucia)_
